#pragma once

#include <iostream>
#include <string>
#include "Character.h"
#include "Room.h"

class Player : public Character {
private:
	Room* location;

public:
	Player(const std::string& name, int health);

	Room* GetLocation();
	void SetLocation(Room* room);
	
	void Look();
	void Interact(const std::string& itemName);
	void Pickup(const std::string& itemName, Player* player);
	void Move(const std::string& direction);
};

