#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

class Character {
private:
	std::string name;
	int health;
	std::vector<Item> inventory;

public:
	Character(const std::string& name, int health);

	void TakeDamage(int damage);
	void AddItem(const Item& item);
	void RemoveItem(const Item& item);
	std::vector<Item> GetInventory();
};
