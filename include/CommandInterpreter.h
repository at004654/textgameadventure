#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include "Player.h"

class CommandInterpreter {
public:
	CommandInterpreter(Player* player);
	void interpretCommand(const std::string& command);
private:
	Player* player_; // Pointer to the player object
};
