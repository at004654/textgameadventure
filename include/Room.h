#pragma once

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include "Item.h"

class Room {
private:
	std::string description;
	std::map<std::string, Room*> exits;
	std::map<std::string, bool> doors;
	std::vector<Item> items;

public:
	Room(const std::string& desc);

	std::string GetDescription();
	std::map<std::string, Room*> GetExits();
	std::vector<Item> GetItems();

	void AddItem(Item item);
	void RemoveItem(const Item& item);
	void AddExit(const std::string& direction, Room* exitRoom);
	Room* GetExit(const std::string& direction);
	void AddDoor(const std::string& direction);
	void RemoveDoor(const std::string& direction);
	bool GetDoor(const std::string& direction);
};
