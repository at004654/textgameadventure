#pragma once

#include <iostream>
#include <string>

class Player;

class Item {
private:
	std::string name;
	std::string description;
	Player* belongsTo;

public:
	Item(const std::string& name, const std::string& desc);

	virtual void Interact();
	std::string GetName();
	std::string GetDescription();
	void setBelongsTo(Player* player);
	Player* getBelongsTo();

	bool operator==(const Item& other) const;
};
