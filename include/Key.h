#pragma once

#include <iostream>
#include <string>
#include "Item.h"
#include "Room.h"

class Key : public Item {
public:
	Key(const std::string& name, const std::string& desc);

	void Interact() override;
};
