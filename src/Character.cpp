#include "../include/Character.h"

Character::Character(const std::string& name, int health) : name(name), health(health), inventory() {

}

void Character::TakeDamage(int damage) {
	health -= damage;
}

void Character::AddItem(const Item& item) {
	inventory.push_back(item);
}

void Character::RemoveItem(const Item& item) {
	auto it = std::find(inventory.begin(), inventory.end(), item);

	if (it != inventory.end()) {
		inventory.erase(it);
	}
}

std::vector<Item> Character::GetInventory() {
	return inventory;
}
