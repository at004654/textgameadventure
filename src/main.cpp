#include <iostream>
#include <vector>
#include <map>
#include "../include/Item.h"
#include "../include/Player.h"
#include "../include/Area.h"
#include "../include/CommandInterpreter.h"
#include "../include/Key.h"

int main() {
	// load map
	Area gameWorld;
	gameWorld.LoadMapFromFile("assets/levels/game_map.txt");
	// create items
	Key key("Key", "A shiny key that looks important.");
	gameWorld.GetRoom("startRoom")->AddItem(key);
	gameWorld.GetRoom("startRoom")->AddDoor("north");
	// setup player
	Player player("Alice", 100);
	Room* currentRoom = gameWorld.GetRoom("startRoom");
	player.SetLocation(currentRoom);
	// initialise interpreter
	CommandInterpreter interpreter(&player);
	std::string command;

	// Game loop (basic interaction)
	while (true) {
		std::cout << "Current Location: " << player.GetLocation()->GetDescription() << "\n";
		std::cout << "Items in the room:\n";
		for (Item item : player.GetLocation()->GetItems()) {
			std::cout << "- " << item.GetName() << ": " << item.GetDescription() << "\n";
		}

		std::cout << "commands: ";
		std::cout << "Look around: 'look' | ";
		std::cout << "Interact with an item: e.g. 'interact Key' | ";
		std::cout << "Pick up an item: e.g. 'pickup Sword' | ";
		std::cout << "Move to another room: e.g. 'move north' | ";
		std::cout << "Quit: 'quit'\n";

		std::cout << "> ";
		std::getline(std::cin, command);

		if (command == "quit") {
			break;
		}

		interpreter.interpretCommand(command);
	}

	return 0;
}
