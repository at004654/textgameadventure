#include "../include/Room.h"

// init class
Room::Room(const std::string& desc) : description(desc), exits(), doors(), items() {

}

void Room::AddItem(Item item) {
	items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
	auto it = std::find(items.begin(), items.end(), item);

	if (it != items.end()) {
		items.erase(it);
	}
}

void Room::AddExit(const std::string& direction, Room* exitRoom) {
	exits[direction] = exitRoom;
}

std::string Room::GetDescription() {
	return description;
}

std::map<std::string, Room*> Room::GetExits() {
	return exits;
}

std::vector<Item> Room::GetItems() {
	return items;
}

Room* Room::GetExit(const std::string& direction) {
	return exits[direction];
}

void Room::AddDoor(const std::string& direction) {
	doors[direction] = true;
}

void Room::RemoveDoor(const std::string& direction) {
	doors[direction] = false;
}

bool Room::GetDoor(const std::string& direction) {
	return doors[direction];
}
