#include "../include/Player.h"

Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {
	
}

Room* Player::GetLocation() {
	return location;
}

void Player::SetLocation(Room* room) {
	location = room;
}

void Player::Look() {
	std::cout << "You look around the room.\n";
}

void Player::Interact(const std::string& itemName) {
	for (Item& item : GetInventory()) {
		if (item.GetName() == itemName) {
			item.Interact();
			break;
		}
	}
}

void Player::Pickup(const std::string& itemName, Player* player) {
	for (Item& item : GetLocation()->GetItems()) {
		if (item.GetName() == itemName) {
			AddItem(item);
			GetLocation()->RemoveItem(item);
			item.setBelongsTo(player);
			break;
		}
	}
}

void Player::Move(const std::string& direction) {
	Room* nextRoom = GetLocation()->GetExit(direction);
	if (GetLocation()->GetDoor(direction) == true) {
		std::cout << "This exit is locked by a door.\n";
	}
	else if (nextRoom != nullptr) {
		SetLocation(nextRoom);
		std::cout << "You move to the next room.\n";
	}
	else {
		std::cout << "You can't go that way.\n";
	}
}
