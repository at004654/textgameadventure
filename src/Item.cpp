#include "../include/Item.h"
#include "../include/Player.h"

Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc), belongsTo(nullptr) {
	
}

void Item::Interact() {
	std::cout << "You interacted with the " << name << ".\n";
}

std::string Item::GetName() {
	return name;
}

std::string Item::GetDescription() {
	return description;
}

void Item::setBelongsTo(Player* player) {
	belongsTo = player;
}

Player* Item::getBelongsTo() {
	return belongsTo;
}

bool Item::operator==(const Item& other) const {
	return (this->name == other.name);
}
