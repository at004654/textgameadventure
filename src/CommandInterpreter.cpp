#include "../include/CommandInterpreter.h"

CommandInterpreter::CommandInterpreter(Player* player) : player_(player) {

}

void CommandInterpreter::interpretCommand(const std::string& command) {
	std::istringstream commandStream(command);
	std::string word;
	commandStream >> word;

	if (word == "look") {
		player_->Look();
	}
	else if (word == "interact") {
		commandStream >> word;
		player_->Interact(word);
	}
	else if (word == "pickup") {
		commandStream >> word;
		player_->Pickup(word, player_);
	}
	else if (word == "move") {
		commandStream >> word;
		player_->Move(word);
	}
	else {
		std::cout << "Unknown command: " << command << "\n";
	}
}
