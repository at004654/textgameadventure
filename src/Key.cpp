#include "../include/Key.h"
#include "../include/Player.h"

Key::Key(const std::string& name, const std::string& desc) : Item(name, desc) {

}

void Key::Interact() {
	std::cout << "What direction do you want to use your key (e.g. north): ";
	std::string direction;
	std::cin >> direction;
	
	getBelongsTo()->GetLocation()->RemoveDoor(direction);
}
