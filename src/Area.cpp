#include "../include/Area.h"

Area::Area() : rooms() {

}

void Area::AddRoom(const std::string& name, Room* room) {
	rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) {
	return rooms[name];
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
	Room* room1 = rooms[room1Name];
	Room* room2 = rooms[room2Name];
	room1->AddExit(direction, room2);
	
	if (direction == "north") {
		room2->AddExit("south", room1);
	}
	else if (direction == "east") {
		room2->AddExit("west", room1);
	}
	else if (direction == "south") {
		room2->AddExit("north", room1);
	}
	else if (direction == "west") {
		room2->AddExit("east", room1);
	}
}

void Area::LoadMapFromFile(const std::string& fileName) {
	std::ifstream mapFile(fileName);

	std::string line;
	while (std::getline(mapFile, line)) {
		std::istringstream iss(line);
		std::string roomName;
		std::string roomDescription;
		std::string connectedRoomName;
		std::string connectedRoomDescription;
		std::string direction;

		std::getline(iss, roomName, '|');
		std::getline(iss, roomDescription, '|');
		std::getline(iss, connectedRoomName, '|');
		std::getline(iss, connectedRoomDescription, '|');
		std::getline(iss, direction);

		Room* room = nullptr;
		Room* connectedRoom = nullptr;

		auto roomIt = rooms.find(roomName);
		if (roomIt == rooms.end()) {
			room = new Room(roomDescription);
			rooms[roomName] = room;
		}
		else {
			room = roomIt->second;
		}

		auto connectedRoomIt = rooms.find(connectedRoomName);
		if (connectedRoomIt == rooms.end()) {
			connectedRoom = new Room(connectedRoomDescription);
			rooms[connectedRoomName] = connectedRoom;
		}
		else {
			connectedRoom = connectedRoomIt->second;
		}

		room->AddExit(direction, connectedRoom);
	}

	mapFile.close();
}
